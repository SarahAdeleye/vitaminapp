# VitaminApp

This is an android app that allows users to manage their vitamins. Users can set timers to remind them to take their vitamins. The app also allows users to view their vitamins in a table and add new entries into the table. I've included a video demonstration of the app

